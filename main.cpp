#include <QApplication>
#include "trueCostDialog.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
	
	TrueCostDialog tcd;
	tcd.setAttribute(Qt::WA_QuitOnClose);
	tcd.show();
						
    return a.exec();
}
