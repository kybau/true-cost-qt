#ifndef TRUE_COST_CALCULATOR_H
#define TRUE_COST_CALCULATOR_H

#include <QObject>
#include <QString>

class TrueCostCalculator : public QObject
{
	Q_OBJECT
	
private:
	double wage;
	double price;
	
	int days;
	int hours;
	int minutes;
	
	QString dayStr, hourStr, minuteStr;
	
public:
	TrueCostCalculator(QObject* = 0);
	void updateLabels();
	
signals:
    void daysChanged(const QString&);
    void hoursChanged(const QString&);
    void minutesChanged(const QString&);
	
public slots:
	void setWage(const QString& str);
	void setPrice(const QString& str);
    bool calculate();
	void clear();
};

#endif