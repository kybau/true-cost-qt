#include "trueCostCalculator.h"
#include <QDebug>
#include <iostream>


TrueCostCalculator::TrueCostCalculator(QObject* parent) :
	QObject(parent)
{
	wage = 0;
	price = 0;
	
	days = 0;
	hours = 0;
	minutes = 0;
	
	dayStr = "";
	hourStr = "";
	minuteStr = "";
	
}

bool TrueCostCalculator::calculate()
{
	//Reset previous values
	days = 0;
	hours = 0;
	minutes = 0;
	
	if (price == 0 || wage == 0)
		return false;
	
	double hoursRequired = 0;
	hoursRequired = price / wage;
	
    while (hoursRequired >= 24)
    {
        hoursRequired = hoursRequired - 24;
        days++;
    }
	
	//Sets the truncated value of hoursRequired to hours, resulting in a whole number
	hours = hoursRequired; 
	
	//Sets minutes to the result of subtracting hours from hoursRequired, resulting in a
	//decimal number representing a fraction of an hour. This can be multiplied by 60 to get
	//the actual amount of minutes.
	minutes = (hoursRequired - hours) * 60;	
	
	qDebug() << "Calculation Complete.";
	qDebug() << "Days: " << days;
	qDebug() << "Hours: " << hours;
	qDebug() << "Minutes: " << minutes;
	
	updateLabels();

	return true;
}


void TrueCostCalculator::setWage(const QString& str)
{
	qDebug() << "Hourly Wage: " << str;
	bool ok = false;
	
	double tempWage = str.toDouble(&ok);
	if (ok)
	{
		wage = tempWage;
	}
	else
	{
		qDebug() << "Error in TrueCostCalculator::setWage() : String could not be converted into double.";
	}	
}

void TrueCostCalculator::setPrice(const QString& str)
{
	qDebug() << "Item Price: " << str;
	bool ok = false;
	
	double tempPrice = str.toDouble(&ok);
	if (ok)
	{
		price = tempPrice;
	}
	else
	{
		qDebug() << "Error in TrueCostCalculator::setPrice() : String could not be converted into double.";
	}	
}

void TrueCostCalculator::updateLabels()
{
		
	dayStr = QString("%1").arg(days);
	dayStr += " days";
	hourStr = QString("%1").arg(hours);
	hourStr += " hours";
	minuteStr = QString("%1").arg(minutes);
	minuteStr += " minutes";
	
	emit daysChanged(dayStr);
	emit hoursChanged(hourStr);
	emit minutesChanged(minuteStr);
}

void TrueCostCalculator::clear()
{
	wage = 0;
	price = 0;
	
	days = 0;
	hours = 0;
	minutes = 0;

	updateLabels();
}
