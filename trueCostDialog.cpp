#include "trueCostDialog.h"
#include "trueCostCalculator.h"
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QDoubleValidator>
#include <QLabel>
#include <QDebug>

TrueCostDialog::TrueCostDialog()
{	
    QVBoxLayout* mainLayout = new QVBoxLayout(this);
	QVBoxLayout* infoLayout = new QVBoxLayout;
	QHBoxLayout* outputLayout = new QHBoxLayout;
	QHBoxLayout* buttonLayout = new QHBoxLayout;
	mainLayout->addLayout(infoLayout);	
	mainLayout->addLayout(outputLayout);
	mainLayout->addLayout(buttonLayout);
	
	setWindowTitle(tr("True Cost"));
	
	QLabel* label1 = new QLabel(QString("At"));
	QLabel* label2 = new QLabel(QString("an hour, an item costing"));
	QLabel* label3 = new QLabel(QString("will also cost"));
	QLabel* dayLabel = new QLabel(QString("0 days"));
	QLabel* hourLabel = new QLabel(QString("0 hours"));
	QLabel* minuteLabel = new QLabel(QString("0 minutes"));
	
	QLineEdit* hourlyWage = new QLineEdit;
	QLineEdit* itemPrice = new QLineEdit;
	
	QPushButton* calculateButton = new QPushButton(QString("Calculate"), this);
	QPushButton* resetButton = new QPushButton(QString("Reset"), this);
	QPushButton* quitButton = new QPushButton(QString("Quit"), this);
	
	infoLayout->addWidget(label1);
	infoLayout->addWidget(hourlyWage);
	infoLayout->addWidget(label2);
	infoLayout->addWidget(itemPrice);
	infoLayout->addWidget(label3);
	infoLayout->addSpacing(15);
	
	outputLayout->addWidget(dayLabel);
	outputLayout->addWidget(hourLabel);
	outputLayout->addWidget(minuteLabel);
	
	mainLayout->insertSpacing(2, 25);
	
	buttonLayout->addWidget(calculateButton);
	buttonLayout->addWidget(resetButton);
	buttonLayout->addWidget(quitButton);
	
	QDoubleValidator* hourlyWageValidator = new QDoubleValidator(1, 9999, 2, this);
	hourlyWageValidator->setNotation(QDoubleValidator::StandardNotation);
	hourlyWage->setValidator(hourlyWageValidator);
	
	QDoubleValidator* itemPriceValidator = new QDoubleValidator(1, 9999999, 2, this);
	itemPriceValidator->setNotation(QDoubleValidator::StandardNotation);
	itemPrice->setValidator(itemPriceValidator);
	
	TrueCostCalculator *tcc = new TrueCostCalculator(this);
	
    connect(quitButton, SIGNAL(clicked()),
			this, SLOT(accept()));
						
	connect(resetButton, SIGNAL(clicked()),
					hourlyWage, SLOT(clear()));
	connect(resetButton, SIGNAL(clicked()),
					itemPrice, SLOT(clear()));
					
	connect(resetButton, SIGNAL(clicked()),
					tcc, SLOT(clear()));
	connect(resetButton, SIGNAL(clicked()),
					tcc, SLOT(clear()));
														
	connect(hourlyWage, SIGNAL(textChanged(const QString&)), 
			tcc, SLOT(setWage(const QString&)));
	connect(itemPrice, SIGNAL(textChanged(const QString&)), 
			tcc, SLOT(setPrice(const QString&)));
	
	connect(calculateButton, SIGNAL(clicked()), 
			tcc, SLOT(calculate()));
			
	connect(tcc, SIGNAL(daysChanged(const QString&)),
            dayLabel, SLOT(setText(const QString&)));
	connect(tcc, SIGNAL(hoursChanged(const QString&)),
            hourLabel, SLOT(setText(const QString&)));
	connect(tcc, SIGNAL(minutesChanged(const QString&)),
            minuteLabel, SLOT(setText(const QString&)));
				
}